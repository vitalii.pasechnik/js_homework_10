"use strict";

const tabs = document.body.querySelectorAll('.tabs-title');
const tabsContent = document.body.querySelector('.tabs-content');
const initialText = Array.from(tabsContent.children).map(el => el.textContent);

window.addEventListener("click", (e) => outsideClick(e));
tabs.forEach(tab => tab.addEventListener('click', (e) => showTabContent(e)));

const showTabContent = (e) => {
    tabsContent.innerHTML = '';
    tabs.forEach((tab, tabInd) => {
        const tabContentContainer = document.createElement('li');
        tabsContent.append(tabContentContainer);
        const tabContent = initialText[tabInd];
        if (tab === e.target && !tab.classList.contains('active')) {
            tabContentContainer.innerText = '';
            tab.classList.add("active");
            tabContentContainer.classList.add("active");
            tabContent.split('').forEach((letter, i) => setTimeout(() => tabContentContainer.append(letter), 1.5 * i));
        } else {
            tab.classList.remove("active");
            tabContentContainer.classList.remove("active");
        };
    });
}

const outsideClick = (e) => {
    if (e.target.closest(".tabs-content") === null && !e.target.classList.contains('tabs-title')) {
        Array.from(tabsContent.children).forEach(el => el.classList.remove("active"));
        tabs.forEach(tab => tab.classList.remove("active"));
    }
}

// const tabs = document.body.querySelector('.tabs');
// const tabsContent = document.body.querySelector('.tabs-content');
// const initialText = [...tabsContent.children].map(el => el.textContent);
// window.addEventListener("click", (e) => outsideClick(e));
// tabs.addEventListener('click', (e) => showTabContent(e));

// const showTabContent = (e) => {
//     e.target.classList.toggle('active');
//     const tabInd = [...tabs.children].indexOf(e.target);
//     [...tabsContent.children].forEach(el => el.innerText = '');
//     [...tabs.children].forEach(tab => tab !== e.target && tab.classList.remove("active"));
//     [...tabsContent.children].forEach((li, ind) => ind !== tabInd && li.classList.remove("active"));
//     tabsContent.children[tabInd].classList.toggle("active");
//     initialText[tabInd].split('').forEach((letter, i) => setTimeout(() => tabsContent.children[tabInd].append(letter), 1.5 * i));
// }

// const outsideClick = (e) => {
//     if (e.target.closest(".tabs-content") === null && !e.target.classList.contains('tabs-title')) {
//         [...tabsContent.children].forEach(el => el.classList.remove("active"));
//         [...tabs.children].forEach(tab => tab.classList.remove("active"));
//     }
// }